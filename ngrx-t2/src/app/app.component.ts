import { Component, OnInit, ViewChild, ElementRef, Renderer2 } from '@angular/core';
import { Rect } from './rect';
import { Observable } from 'rxjs';
import { Store, select } from '@ngrx/store';
import * as main from './actions/mainchar.action';
import * as mainCharSelector from './selectors/mainchar.selector';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  @ViewChild('canvas', { static: true }) canvas: ElementRef<HTMLCanvasElement>;
  private ctx: CanvasRenderingContext2D;
  requestId: any;
  rect: any;
  ///////////////////////////
  right = false;
  left = false;
  ///////////////////////////
  title = 'ngrx-t2';
  ///////////////////////////
  mainChar$: Observable<any>;
  speed$: Observable<number>;

  constructor(
    public render: Renderer2,
    private store: Store<{ mainChar: any }>
  ) {

  }

  ngOnInit() {
    this.mainChar$ = this.store.pipe(select('mainChar'));
    this.speed$ = this.store.pipe(select(mainCharSelector.getSpeed));
    
    this.ctx = this.canvas.nativeElement.getContext('2d');
    this.rect = new Rect(this.ctx, 0, 50, 100, 'red');

    setInterval(() => {
      this.draw();
    }, 1);

    /* Render */
    this.render.listen('document', 'keydown', (e: any) => {
      if (e.key === 'ArrowRight') {
        this.right = true;
      } else if (e.key === 'ArrowLeft') {
        this.left = true;
      }
    });

    this.render.listen('document', 'keyup', (e: any) => {
      this.right = false;
      this.left = false;
    });
  }

  public draw() {
    this.ctx.clearRect(0, 0, this.ctx.canvas.width, this.ctx.canvas.height);
    this.ctx.fillStyle = 'black';
    this.ctx.fillRect(0, this.canvas.nativeElement.height - 40, this.canvas.nativeElement.width, 40);

    if (this.right) {
      this.rect.moveRight();
    } else if (this.left) {
      this.rect.moveLeft();
    }

    this.rect.draw();

    this.speed$ = this.store.pipe(select(mainCharSelector.getSpeed));
    
    this.requestId = requestAnimationFrame(() => this.draw);
  }

  public normal() {
    this.store.dispatch(main.normal());
    this.mainChar$.subscribe(res => {
      console.log(res);
      this.rect.speed = res.speed;
      this.rect.w = res.w;
      this.rect.h = res.h;
      this.rect.color = res.color;
    });
  }

  public speed(s: number) {
    this.store.dispatch(main.speed({s}));
    this.mainChar$.subscribe(res => {
      console.log(res);
      this.rect.speed = res.speed;
      this.rect.w = res.w;
      this.rect.h = res.h;
      this.rect.color = res.color;
    });
  }

  public slow(s: number) {
    this.store.dispatch(main.slow({s}));
    this.mainChar$.subscribe(res => {
      console.log(res);
      this.rect.speed = res.speed;
      this.rect.w = res.w;
      this.rect.h = res.h;
      this.rect.color = res.color;
    });
  }
}
