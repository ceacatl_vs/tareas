import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { DestinoViaje } from '../models/destinoViaje.model';
import { Store } from '@ngrx/store';
import { AppState } from '../app.module';
import { NuevoDestinoAction, QuitarDestinoAction, ElegirDestinoAction } from '../models/destinos-state.model';

@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.css']
})
export class ListaDestinosComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<DestinoViaje>;

  destinos: DestinoViaje[];

  constructor(
    private store: Store<AppState>
  ) {
    this.onItemAdded = new EventEmitter();
    this.destinos = [];

    this.store.select(state => state.destinos.items)
      .subscribe(items => this.destinos = items);
  }

  ngOnInit(): void {
  }

  guardar(destino: DestinoViaje) {
    this.store.dispatch(new NuevoDestinoAction(destino));

    this.onItemAdded.emit(destino);
  }

  quitar(destino: DestinoViaje) {
    this.store.dispatch(new QuitarDestinoAction(destino));
  }

  seleccionado(d: DestinoViaje) {
    this.store.dispatch(new ElegirDestinoAction(d));
  }

}
