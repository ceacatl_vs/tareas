import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { DestinoViaje } from '../models/destinoViaje.model';
import { FormGroup, FormBuilder, FormControl, Validators, ValidatorFn } from '@angular/forms';

@Component({
  selector: 'app-form-destino',
  templateUrl: './form-destino.component.html',
  styleUrls: ['./form-destino.component.css']
})
export class FormDestinoComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<DestinoViaje>;

  fg: FormGroup;
  restriccion = "antartida";

  constructor(fb: FormBuilder) {
    this.onItemAdded = new EventEmitter();
    this.fg = fb.group({
      destino: ['', Validators.compose([
        Validators.required,
        this.longitudValidator,
        this.restriccionValidator(this.restriccion)
      ])],
      pais: ['']
    });

    this.fg.valueChanges.subscribe((form: any) => {
      console.log('Ha cambiado el formulario: ', form);
    })
  }

  ngOnInit(): void {
  }

  guardar(nombre: string, pais: string) {  
    const destino = new DestinoViaje(nombre, pais);
    this.onItemAdded.emit(destino);
    return false;
  }

  longitudValidator(control: FormControl): { [s: string]: boolean } {
    const l = control.value.toString().trim().length;

    if (l > 0 && l < 3) {
      return {
        invalidLong: true
      }
    }

    return null;
  }

  restriccionValidator(nombre: string): ValidatorFn {
    return (control: FormControl): { [s: string]: boolean } | null => {
      const n = control.value.toString().trim();

      if (n === nombre) {
        return { restrictedDestino: true }
      }

      return null;
    }
  }

}
