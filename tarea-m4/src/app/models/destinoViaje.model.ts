import { v4 as uuid } from 'uuid';

export class DestinoViaje {
    id = uuid();
    selected: boolean;
    
    constructor(public nombre: string, public pais: string, public votes: number = 0) {
           
    }

    voteUp() {
        this.votes++;
    }

    voteDown() {
        if(this.votes > 0) {
            this.votes--;
        }
    }
}