import {
    destinosReducer,
    DestinosState,
    initialDestinosState,
    InitMyDataAction,
    NuevoDestinoAction
  } from './destinos-state.model';
  import { DestinoViaje } from './destinoViaje.model';
  
  describe('reducerDestinosViajes', () => {
    it('should reduce init data', () => {
      // setup
      const prevState: DestinosState = initialDestinosState();
      const action: InitMyDataAction = new InitMyDataAction(['destino 1', 'destino 2']);
      // action
      const newState: DestinosState = destinosReducer(prevState, action);
      // assert
      expect(newState.items.length).toEqual(2);
      expect(newState.items[0].nombre).toEqual('destino 1');
      // tear down
    });
  
    it('should reduce new item added', () => {
      const prevState: DestinosState = initialDestinosState();
      const action: NuevoDestinoAction = new NuevoDestinoAction(new DestinoViaje('barcelona', 'url'));
      const newState: DestinosState = destinosReducer(prevState, action);
      expect(newState.items.length).toEqual(1);
      expect(newState.items[0].nombre).toEqual('barcelona');
    });
  });
