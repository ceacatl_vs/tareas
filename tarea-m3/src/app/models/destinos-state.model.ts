import { DestinoViaje } from './destinoViaje.model';
import { Action } from '@ngrx/store';

// ESTADO
export interface DestinosState {
    items: DestinoViaje[];
    favorito: DestinoViaje;
}

export function initialDestinosState() {
    return {
        items: [],
        favorito: null
    };
}

// ACCIONES
export enum DestinosActionTypes {
    INIT_MY_DATA = '[Destinos] Init My Data',
    NUEVO_DESTINO = '[Destinos] Nuevo',
    ELEGIR_DESTINO = '[Destinos] Elegir',
    QUITAR_DESTINO = '[Destinos] Quitar',
    VOTE_UP = '[Destinos] Vote Up',
    VOTE_DOWN = '[Destinos] Vote Down'
}

export class InitMyDataAction implements Action {
    type = DestinosActionTypes.INIT_MY_DATA;
    constructor(public destinos: string[]) {}
}

export class NuevoDestinoAction implements Action {
    type = DestinosActionTypes.NUEVO_DESTINO;
    constructor(public destino: DestinoViaje) {}
}

export class ElegirDestinoAction implements Action {
    type = DestinosActionTypes.ELEGIR_DESTINO;
    constructor(public destino: DestinoViaje) {}
}

export class QuitarDestinoAction implements Action {
    type = DestinosActionTypes.QUITAR_DESTINO;
    constructor(public destino: DestinoViaje) {}
}

export class VoteUpAction implements Action {
    type = DestinosActionTypes.VOTE_UP;
    constructor(public destino: DestinoViaje) {}
}

export class VoteDownAction implements Action {
    type = DestinosActionTypes.VOTE_DOWN;
    constructor(public destino: DestinoViaje) {}
}

export type DestinosActions = InitMyDataAction | NuevoDestinoAction | QuitarDestinoAction
    | ElegirDestinoAction | VoteUpAction | VoteDownAction;

// REDUCERS
export function destinosReducer(
    state: DestinosState,
    action: DestinosActions
): DestinosState {
    switch (action.type) {
        case DestinosActionTypes.INIT_MY_DATA: {
            const destinos: string[] = (action as InitMyDataAction).destinos;
            return {
                ...state,
                items: destinos.map((d) => new DestinoViaje(d, ''))
            };
        }
        case DestinosActionTypes.NUEVO_DESTINO: {
            console.log('destinosReducer - NUEVO_DESTINO');
            return {
                ...state,
                items: [...state.items, (action as NuevoDestinoAction).destino],
                favorito: state.favorito
            };
        }
        case DestinosActionTypes.ELEGIR_DESTINO: {
            console.log('reducerDestinosViajes - ELEGIDO_FAVORITO');
            let destinos = state.items.map(x => ({...x}));
            destinos.forEach(x => x.selected = false);
            console.log(destinos);
            let fav: DestinoViaje = <DestinoViaje>destinos.find(d => d.id === (action as ElegirDestinoAction).destino.id);
            console.log(fav);
            fav.selected = true;
            console.log(fav);
            return {
                ...state,
                items: <DestinoViaje[]>destinos,
                favorito: fav
            };
        }
        case DestinosActionTypes.QUITAR_DESTINO: {
            console.log('destinosReducer - QUITAR_DESTINO');
            let destinos = state.items.map(x => ({...x}));
            console.log(destinos);
            const d: DestinoViaje = (action as QuitarDestinoAction).destino;
            console.log(d);
            const index = destinos.map(function(i){ return i.id; }).indexOf(d.id);
            console.log(index);
            destinos.splice(index, 1);
            console.log(destinos);
            return {
                ...state,
                items: <DestinoViaje[]>destinos
            }
        }
        case DestinosActionTypes.VOTE_UP: {
            console.log('destinosReducer - VOTE_UP');
            let destinos = state.items.map(x => ({...x}));
            let d: DestinoViaje = <DestinoViaje>destinos.find(d => d.id === (action as VoteUpAction).destino.id);
            d.votes++;
            console.log(state);
            return {
                ...state,
                items: <DestinoViaje[]>destinos
            };
        }
        case DestinosActionTypes.VOTE_DOWN: {
            console.log('destinosReducer - VOTE_DOWN');
            let destinos = state.items.map(x => ({...x}));
            let d: DestinoViaje = <DestinoViaje>destinos.find(d => d.id === (action as VoteDownAction).destino.id);
            if(d.votes > 0) {
                d.votes--;
            }
            console.log(state);
            console.log(d);
            return {
                ...state,
                items: <DestinoViaje[]>destinos
            };
        }
    }

    return state;
}
